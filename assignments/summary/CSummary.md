## C PROGRAMMING:
        The C language was developed by Bjarne Stroustrup and Dennis Ritchie at AT&T bell laboratories. It is a procedure oriented language 
        in which programs are divided into small parts called functions.
## Structure of a C program:
        A C program consists of the following parts. They are,
            1. Preprocessor Directives 
            2. Variables
            3. functions
            4. Expressions
            5. Statements
            6. Comments 
## Compile and Execute a C program:
        1. Open a text editor
        2. Type the program
        3. Save the program with a **.c** extension
        4. In the terminal, type **gcc progname.c**
        5. After successful compilation without errors, type **"./a.out"**
        6. The program will be executed
## Basic Syntax:
        1. Semicolon(;) which is a statement terminator.
        2. Comments(/**/) which are the helping text in a program.
        3. Identifiers are names used to identify a variable, a function or any user defined item. They usually start with an alphabet or an
           underscore.
        4. Keywords are the reserved words specific to a C program. Some keywords are for, while, if etc.
        5. Whitespaces are tabspaces and blanklines.
## Data types:
        1. Basic types which are arithmetic data types. They are int and float.
        2. Enumerated types are also arithmetic data types in which they are used to define variables that can only be assigned to certain 
           integral values.
        3. Void types are types which indicate no value is available.
        4. Pointers, Arrays, Structures and Unions are derived data types.
## Variables:
        Variables are containers which are used to store a value or data. A variable can be declared by the syntax **return_type var_name;**
## Constants:
        Constants are values which will not change any time. It can be defined using #define and const keyword.
## Storage class:
        1. Auto: Default storage class in which its scope exists in the declared block only.
        2. Static: Storage class in which the variables exist permanent throughout the lifetime of the program
        3. Extern: Storage class in which variables visible to all program files. It is used only when a global variable is used in another file
        4. Register: Storage class in which variables are defined in the register instead of RAM. The addressof(&) operator cannot be used as it 
                     doesn't contains an address.
## Decision making:
        1. if [if(condition){statements}]
        2. if-else if[if(condition){statements} else if(condition){}]
        3. if-else[if(condition){statements} else{statements}]
        4. Nested if
        5. Switch Statements[switch(value){case 1:statements;break;...case n:statements;break; default:statements;break;}]
        6. Nested switch statements.
## Loops:
        1. while(condition){}
        2. do{}while(condition)
        3. for loop. Endless condition for(;;)
## Functions:
        Group of statements that performs a certain task. 
        Syntax for function declaration is return_type func_name(parameter list){} or return_type func_name(parameter list);
        Syntax for function call is func_name(parameter to be passed);
## Function calls:
        1. Call by value: Function calls in which the formal parameters are passed
        2. Call by reference: Function class in which the address of the parameters are passed.
## Scope of variables:
        1. Local: Scope is within the block itself
        2. Global: Scope will be throughout the program's lifetime
## Pointers:
        Pointers refers to the address of a variable. A pointer variable stores the memory address of the variable.
        The & operator is used to get the address of a variable.
        The pointer variable declaration syntax is data_type *ptr_name;
## Arrays:
        Arrays are derived data types in which the data of the same data type are stored in contiguous memory locations.
        The declaration of an array is data_type arr_name[size];
        The array elements can also be accessed by *(arr_name+index)
        There are 2d arrays also, which can be declared by data_type arr_name[size1][size2];
## Strings:
        A string is a one dimensional array in which the terminal character will be '/0'.
        In C, the string can be declared using char array by char str_name[]="string";
        Functions supported for string are:
        1. strcpy(s1,s2):Copies string s2 into s1
        2. strcat(s1,s2):Concatenates string s2 at the end of string s1
        3. strlen(s1):Returns the length of string s1
        4. strcmp(s1,s2):Returns 0 when both strings s1 and s2 are same
                         Returns a value less than 0 when s1<s2.
                         Returns a value greater than 0 when s1>s2.
        5. strchr(s1,ch):Returns a pointer to the first occurence of character ch in string s1
        6. strstr(s1,s2):Returns a pointer to the first occurence of string s2 in string s1.
## Structures:
        C Structures are derived data types which can hold values of different data types.
        The syntax to define a structure is
        struct struct_name{
                data types;
        };
        The structure members can be accessed using a dot(.) operator.
        A struct pointer can also be defined and the address of the structure can be stored in it.
        Accessing of structure members using a pointer can be done by (->) operator.
## typedef:
        typedef is a keyword in C which gives a new name to the type. It follows scope rules .(ie), the 
        same name can be given in a different block and we can use it there too.
